# slipperybot

a discord bot written in rust. it sucks and i'm the only one who can use it, so go away. don't even read me.

comes with some tools for playing with the bot's features independently.

### assets

* ffmpeg: https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full-shared.7z
  * unzip into ./data/ffmpeg
* hl1 sounds: https://github.com/sourcesounds/hl1/tree/master
  * you'll need fvox, hgrunt, and vox. unzip into ./data/hl1