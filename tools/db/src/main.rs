use std::env;
use dotenvy::dotenv;
use diesel::prelude::*;
use db::rpg::character;

fn main() {
    dotenv()
        .expect(".env file not found");

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let mut conn = SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));

    let char = character::get_character(&mut conn, 282789048265736192);

    if let Some(c) = char {
        println!("{:#?}", c);
    } else {
        println!("character not found.");
    }
}
