use std::fs;
use std::io::Write;

fn main() -> std::io::Result<()> {
    let corpus_files = fs::read_dir("data/corpus").unwrap();

    let mut target = fs::File::create("data/training_data.txt")?;

    for cf in corpus_files {
        let path = cf.unwrap().path();

        if !path.is_dir() {
            let contents = fs::read_to_string(path).expect("Could not open file");

            target.write(
                contents
                    .to_lowercase()
                    .trim_end()
                    .replace("\n\n", "\n")
                    .replace("\n ", "\n")
                    .as_bytes(),
            )?;
        }
    }

    println!(".: wrote corpus to data/training_data.txt");

    Ok(())
}
