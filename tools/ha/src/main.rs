use dotenvy::dotenv;
use backend::rest::homeassistant::HomeAssistantClient;

#[tokio::main]
async fn main() {
    dotenv().expect(".env file not found");

    let ha_client = HomeAssistantClient::new();
    let response = ha_client.toggle_area("living_room".to_string()).await;

    match response {
        Ok(r) => println!("{:#?}", r),
        Err(e) => eprintln!("{}", e),
    }
}
