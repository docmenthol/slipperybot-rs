use backend::cr;
use rustkov::prelude::{Brain, BrainConfig};
use std::io::Write;
use std::path::Path;
use std::time::Duration;
use terminal::Event;
use terminal::KeyCode;
use terminal::{error, Action, Clear, Retrieved, Value};

pub fn main() -> error::Result<()> {
    let mut terminal = terminal::stdout();
    let mut typed: String = "".to_string();

    if let Retrieved::TerminalSize(_, y) = terminal.get(Value::TerminalSize)? {
        terminal.act(Action::ClearTerminal(Clear::All))?;

        terminal.batch(Action::MoveCursorTo(0, y - 2))?;
        terminal.write(b".: slipperybot interact")?;
        terminal.batch(Action::MoveCursorTo(0, y))?;
        terminal.write(b".: loading saved chain... ")?;

        let mut chain = Brain::from_file(Path::new("data/Chain.bin").to_str().unwrap())
            .expect("Failed to load saved chain!");

        chain.config =
            BrainConfig::from_file("data/Chain.toml").expect("Could not load brain config.");

        terminal.write(b"done.")?;
        cr!(terminal, y);
        terminal.write(b"(Ctrl+C to exit)")?;
        terminal.batch(Action::ScrollUp(1))?;
        cr!(terminal, y);
        terminal.write(b"> ")?;

        terminal.flush_batch()?;

        loop {
            if let Retrieved::Event(event) =
                terminal.get(Value::Event(Some(Duration::from_millis(500))))?
            {
                match event {
                    Some(Event::Key(key)) => {
                        if key.code == KeyCode::Enter {
                            terminal.batch(Action::MoveCursorTo(0, y))?;
                            terminal.batch(Action::ClearTerminal(Clear::FromCursorDown))?;

                            terminal.write(format!("ME: {}", typed).as_bytes())?;
                            cr!(terminal, y);

                            let reply = chain
                                .generate(typed)
                                .expect("could not generate reply")
                                .unwrap();
                            terminal.write(format!("BOT: {}", reply).as_bytes())?;

                            terminal.batch(Action::ScrollUp(1))?;
                            cr!(terminal, y);

                            terminal.write(b"> ")?;

                            terminal.flush_batch()?;

                            typed = "".to_string();
                        } else {
                            if let KeyCode::Char(c) = key.code {
                                typed = format!("{}{}", typed, c.to_string());
                                terminal.write(c.to_string().as_bytes())?;
                                terminal.flush_batch()?;
                            }
                        }
                    }
                    _ => {}
                }
            }
        }
    }

    Ok(())
}
