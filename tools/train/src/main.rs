use clap::Parser;
use rustkov::prelude::{Brain, BrainConfig};
use std::path::Path;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Cli {
    #[arg(
        short = 'A',
        long = "add",
        long_help = "Training file to add to existing brain.",
        value_name = "FILE"
    )]
    additional_training: Option<PathBuf>,
    #[arg(short, long = "stats")]
    show_stats: bool,
}

fn main() {
    let args = Cli::parse();

    let chain_file = Path::new("Chain.bin");
    let chain_config =
        BrainConfig::from_file("data/Chain.toml").expect("could not load brain config.");

    if let Some(training_path) = args.additional_training {
        if !chain_file.exists() {
            eprintln!(".: could not find chain. did you train?");
            return;
        }

        let mut chain = Brain::from_file(chain_file.to_str().unwrap())
            .expect("failed to load saved chain! did you train?");

        chain.config = chain_config;

        print!(
            ".: performing additional training with {}... ",
            training_path.display()
        );

        chain.ingest_file(training_path.to_str().unwrap());

        println!("done.");
    } else {
        if chain_file.exists() {
            eprintln!(".: brain already exists. skipping initial training.");
        } else {
            print!(".: performing initial training... ");

            let chain = Brain::new()
                .config(chain_config)
                .expect("Could not apply config.")
                .from_dataset("assets/training_data.txt")
                .expect("Unable to train new chain!")
                .get();

            chain
                .to_file("data/Chain.bin")
                .expect("Unable to save new brain!");

            println!("done.");
        }
    }

    if args.show_stats {
        if !chain_file.exists() {
            eprintln!(".: brain doesn't exist. did you train?");
            return;
        }

        let mut chain = Brain::from_file(chain_file.to_str().unwrap())
            .expect("failed to load saved chain! did you train?");
        let chain_config =
            BrainConfig::from_file("data/Chain.toml").expect("could not load brain config.");

        chain.config = chain_config;

        let stats = chain.stats();

        println!(".: brain knows {} words.", stats.get_total_words());
    }
}
