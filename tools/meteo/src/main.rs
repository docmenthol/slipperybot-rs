use backend::rest::{
    meteo::OpenMeteoClient,
    zippopotamus::ZippopotamusClient
};

#[tokio::main]
async fn main() {
    let om_client = OpenMeteoClient::new();
    let zip_client = ZippopotamusClient::new();

    let location = zip_client.get_coords("us/60651".to_string()).await.unwrap();
    let forecast = om_client.get_forecast(location.latitude, location.longitude).await.unwrap();

    println!("Weather for {}\n------------------\n", location.name);

    let mut hourly_iter = forecast.hourly.clone().into_iter();
    while let Some(hourly) = hourly_iter.next() {
        println!("{}", hourly.datetime.to_rfc2822());

        println!(
            "Temperature: {}{}",
            hourly.temperature_2m,
            forecast.hourly_units.temperature_2m
        );

        println!("Weather: {}", hourly.weather.to_string());

        println!(
            "Rain: {} {}, {}{} chance\n",
            hourly.rain,
            forecast.hourly_units.rain,
            hourly.precipitation_probability,
            forecast.hourly_units.precipitation_probability
        );
    }
}