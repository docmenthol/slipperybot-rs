use backend::audio;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Cli {
    #[arg(long, group = "source")]
    hev: bool,
    #[arg(long, group = "source")]
    vox: bool,
    #[arg(long, group = "source")]
    grunt: bool,

    words: Vec<String>,
}

fn main() {
    let args = Cli::parse();
    let source = if args.vox {
        audio::ClipSource::Vox
    } else if args.grunt {
        audio::ClipSource::Grunt
    } else {
        audio::ClipSource::Hev
    };

    audio::stitch_audio(source, args.words);
}
