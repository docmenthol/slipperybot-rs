use crate::types::Preposition;

#[macro_export]
macro_rules! cr {
    ($t:ident, $y:tt) => {
        $t.batch(Action::ScrollUp(1))?;
        $t.batch(Action::MoveCursorTo(0, $y))?;
    };
}

pub fn parse_on_off(input: &str) -> Option<bool> {
    match input.to_ascii_lowercase().as_str() {
        "on" => Some(true),
        "off" => Some(false),
        _ => None,
    }
}

pub fn parse_preposition(input: &str) -> Option<Preposition> {
    match input.to_ascii_lowercase().as_str() {
        "in" => Some(Preposition::In),
        "at" => Some(Preposition::At),
        _ => None,
    }
}
