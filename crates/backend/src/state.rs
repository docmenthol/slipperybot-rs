extern crate r2d2;
extern crate r2d2_sqlite;
extern crate rusqlite;

use crate::{
    config::Config,
    db::schema,
    types::DbPool,
    rest::homeassistant::HomeAssistantClient
};
use log::debug;
use rustkov::prelude::Brain;
use tokio::sync::Mutex;

#[derive(Debug)]
pub struct State {
    pub pool: DbPool,
    pub config: Mutex<Config>,
    pub chain: Mutex<Brain>,
    pub ha_client: HomeAssistantClient,
}

impl State {
    pub fn new(config: Config, chain: Brain, pool: DbPool) -> Self {
        let conn = pool.get().unwrap();
        schema::create_database(&conn).unwrap();
        debug!("bootstrapped database");

        let config = Mutex::new(config);
        let chain = Mutex::new(chain);

        let ha_client = HomeAssistantClient::new();

        debug!("state created");

        Self {
            pool,
            config,
            chain,
            ha_client
        }
    }
}
