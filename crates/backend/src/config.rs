use serde::Deserialize;
use std::path::PathBuf;

#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    pub server: Server,
    pub features: Features,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Server {
    pub guild: u64,
    pub channel: u64,
    pub admins: Vec<u64>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Features {
    pub introverted: bool,
    pub silent: bool,
}

impl Config {
    pub fn new(path: PathBuf) -> Self {
        toml::from_str(&std::fs::read_to_string(path).unwrap()).unwrap()
    }

    pub fn set_introverted(&mut self, introverted: bool) {
        self.features.introverted = introverted;
    }

    pub fn set_silent(&mut self, silent: bool) {
        self.features.silent = silent;
    }
}
