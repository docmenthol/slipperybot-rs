use std::fs;
use std::path::PathBuf;
use std::process::Command;

#[derive(Clone, Copy)]
pub enum ClipSource {
    Hev,
    Vox,
    Grunt,
}

impl TryFrom<&str> for ClipSource {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "fvox" => Ok(ClipSource::Hev),
            "vox" => Ok(ClipSource::Vox),
            "hgrunt" => Ok(ClipSource::Grunt),
            _ => Err(()),
        }
    }
}

impl From<ClipSource> for &str {
    fn from(value: ClipSource) -> Self {
        match value {
            ClipSource::Hev => "fvox",
            ClipSource::Vox => "vox",
            ClipSource::Grunt => "hgrunt",
        }
    }
}

fn source_path(source: ClipSource) -> PathBuf {
    let d: &str = source.into();
    let source_path = format!("./data/hl1/{}/", d);

    PathBuf::from(source_path)
}

pub fn get_wordlist(source: ClipSource) -> Vec<String> {
    let source_path = source_path(source);
    let word_files = fs::read_dir(source_path).unwrap();
    let words: Vec<String> = word_files
        .into_iter()
        .map(|wf| {
            let path = wf.unwrap().path();
            let stem = path.file_stem().unwrap();
            stem.to_str().unwrap().to_string()
        })
        .filter(|s| s != ".keep")
        .collect();

    words
}

pub fn get_word(source: ClipSource, word: &str) -> Result<PathBuf, String> {
    let mut source_path = source_path(source);
    source_path.push(word);
    source_path.set_extension("wav");

    if source_path.as_path().exists() {
        Ok(source_path)
    } else {
        Err(format!("word \"{}\" not found", word))
    }
}

pub fn stitch_audio(source: ClipSource, words: Vec<String>, fid: String) -> PathBuf {
    let output_filename = format!("data/audio/{}.wav", fid);
    let mut stitch_cmd = Command::new("sox");

    for word in words {
        if let Ok(word_path) = get_word(source, word.as_str()) {
            stitch_cmd.arg(word_path);
        } else {
            let beep = get_word(source, "blip").unwrap();
            stitch_cmd.arg(beep);
        }
    }

    stitch_cmd.arg(output_filename.clone());
    stitch_cmd.output().expect("failed to execute sox");

    Command::new("lame")
        .arg(output_filename.clone())
        .output()
        .expect("failed to run lame");

    fs::remove_file(output_filename).expect("failed to delete temp file");

    PathBuf::from(format!("data/audio/{}.mp3", fid))
}
