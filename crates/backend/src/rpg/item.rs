use std::fmt;

#[derive(Debug)]
pub struct Item {
    pub name: String
}

impl Item {
    pub fn new(name: String) -> Self {
        Self {
            name
        }
    }
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}