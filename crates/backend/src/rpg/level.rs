pub fn level_for_xp(xp: i32) -> i32 {
    let exp = f64::from(xp);
    let level_f = 0.2 * exp + 1.0;
    level_f.floor() as i32
}

pub fn xp_for_level(level: i32) -> i32 {
    let lvl = f64::from(level);
    let exp = lvl / 0.2;
    exp.floor() as i32
}
