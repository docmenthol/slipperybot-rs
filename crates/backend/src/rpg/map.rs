#[derive(Clone, Debug)]
pub struct Map {
    pub id: i32,
    pub name: String,
    pub warps: Vec<Map>,
    pub monster_singular: Option<String>,
    pub monster_plural: Option<String>,
}
