use crate::rpg::{
    map::Map,
    resources::{ore::Ore, tree::Tree},
};
use std::fmt;

#[derive(Clone, Debug)]
pub enum Activity {
    Nothing(Map),
    Fighting(Map),
    Skilling(Map, Skill),
}

#[derive(Clone, Debug)]
pub enum Skill {
    Mining(Ore),
    Chopping(Tree),
    Smithing,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ParsedActivity {
    pub activity_id: i32,
    pub map_id: i32,
    pub skill_id: Option<i32>,
    pub resource_id: Option<i32>,
}

impl Activity {
    fn activity_id(self) -> String {
        match self {
            Activity::Nothing(map) => format!("0,{}", map.id),
            Activity::Fighting(map) => format!("1,{}", map.id),
            Activity::Skilling(map, skill) => match skill {
                Skill::Mining(ore) => format!("2,0,{},{}", map.id, ore.id),
                Skill::Chopping(tree) => format!("2,1,{},{}", map.id, tree.id),
                Skill::Smithing => format!("2,2,{}", map.id),
            },
        }
    }

    fn friendly_string(self) -> String {
        match self {
            Activity::Nothing(_map) => "nothing".to_string(),
            Activity::Fighting(map) => {
                if let Some(monster_plural) = map.monster_plural {
                    format!("fighting {} on {}", monster_plural, map.name)
                } else {
                    format!("fighting something (?) on {}", map.name)
                }
            }
            Activity::Skilling(_map, skill) => skill.to_string(),
        }
    }
}

impl fmt::Display for Skill {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Skill::Mining(ore) => write!(f, "mining {}", ore),
            Skill::Chopping(tree) => write!(f, "chopping {}", tree),
            Skill::Smithing => write!(f, "smithing"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generates_skilling_activity_id_correctly() {
        let skill = Skill::Mining(Ore {
            id: 2,
            name: "copper".to_string(),
        });

        let activity = Activity::Skilling(
            Map {
                id: 1,
                name: "Test Map".to_string(),
                warps: vec![],
                monster_singular: Some("slime".to_string()),
                monster_plural: None,
            },
            skill,
        );

        let expected = "2,0,1,2".to_string();
        assert_eq!(activity.activity_id(), expected);
    }

    #[test]
    fn generates_skilling_friendly_name_correctly() {
        let skill = Skill::Mining(Ore {
            id: 2,
            name: "copper".to_string(),
        });

        let activity = Activity::Skilling(
            Map {
                id: 1,
                name: "Test Map".to_string(),
                warps: vec![],
                monster_singular: Some("slime".to_string()),
                monster_plural: None,
            },
            skill,
        );

        let expected = "mining copper".to_string();
        assert_eq!(activity.friendly_string(), expected);
    }
}
