use crate::rpg::item::Item;

#[derive(Debug)]
pub struct Equipment {
    head: Item,
    body: Item,
    legs: Item,
    feet: Item,
    weapon: Item,
    amulet: Item,
    ring_a: Item,
    ring_b: Item,
}
