use crate::rpg::classes::Class;

#[derive(Clone, Copy, Debug)]
pub struct Stats {
    pub hp: u16,
    pub max_hp: u16,
    pub mana: u16,
    pub max_mana: u16,
    pub str: u16,
    pub agi: u16,
    pub dex: u16,
    pub int: u16,
}

impl Default for Stats {
    fn default() -> Self {
        Self {
            hp: 10,
            max_hp: 10,
            mana: 10,
            max_mana: 10,
            str: 3,
            agi: 3,
            dex: 3,
            int: 3,
        }
    }
}

impl Stats {
    pub fn from(stats: Stats) -> Self {
        Self {
            hp: stats.hp,
            max_hp: stats.max_hp,
            mana: stats.mana,
            max_mana: stats.max_mana,
            str: stats.str,
            agi: stats.agi,
            dex: stats.dex,
            int: stats.int
        }
    }

    pub fn class_initial(class: Class) -> Self {
        let mut stats = Stats::default();

        match class {
            Class::Novice => {}

            Class::Fighter => {
                stats.str = 10;
                stats.agi = 4;
                stats.dex = 6;
            }

            Class::Rogue => {
                stats.str = 6;
                stats.agi = 10;
                stats.dex = 4;
            }

            Class::Archer => {
                stats.agi = 6;
                stats.dex = 10;
                stats.int = 4;
            }

            Class::Mage => {
                stats.agi = 4;
                stats.dex = 6;
                stats.int = 10;
            }
        }

        stats
    }

    pub fn class_upgrade(class: Class, old_stats: Stats) -> Self {
        let mut stats = Stats::from(old_stats);

        match class {
            Class::Novice => {},

            Class::Fighter => {
                stats.str += 7;
                stats.agi += 2;
                stats.dex += 3;
                stats.int += 1;
            }

            Class::Rogue => {
                stats.str += 3;
                stats.agi += 7;
                stats.dex += 2;
                stats.int += 1;
            }

            Class::Archer => {
                stats.str += 1;
                stats.agi += 3;
                stats.dex += 7;
                stats.int += 2;
            }

            Class::Mage => {
                stats.str += 1;
                stats.agi += 2;
                stats.dex += 3;
                stats.int += 7;
            }
        }

        stats
    }
}
