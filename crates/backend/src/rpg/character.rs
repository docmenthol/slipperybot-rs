use crate::rpg::{classes::Class, stats::Stats, item::Item, equipment::Equipment};

#[derive(Debug)]
pub struct Character {
    pub uid: u64,
    pub xp: i32,
    pub class: Class,
    pub stats: Stats,
    pub equipment: Equipment,
    pub inventory: Vec<Item>,
}