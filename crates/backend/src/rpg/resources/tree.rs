use std::fmt;

#[derive(Clone, Debug)]
pub struct Tree {
    pub id: i32,
    pub name: String,
}

impl fmt::Display for Tree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} logs", self.name)
    }
}
