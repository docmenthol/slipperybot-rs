use std::fmt;

#[derive(Clone, Debug)]
pub struct Ore {
    pub id: i32,
    pub name: String,
}

impl fmt::Display for Ore {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ore", self.name)
    }
}
