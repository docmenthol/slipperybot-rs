use r2d2::PooledConnection;
use r2d2_sqlite::SqliteConnectionManager;

const CREATE_DATABASE: &str = "
    BEGIN;
    CREATE TABLE IF NOT EXISTS `characters` (
        `uid` UNSIGNED BIGINT UNIQUE PRIMARY KEY,
        `class` INTEGER NOT NULL,
        `activity` VARCHAR NOT NULL DEFAULT \"0,0\",
        `xp` INTEGER NOT NULL DEFAULT 0,
        `hp` INTEGER NOT NULL DEFAULT 10,
        `max_hp` INTEGER NOT NULL DEFAULT 10,
        `mana` INTEGER NOT NULL DEFAULT 10,
        `max_mana` INTEGER NOT NULL DEFAULT 10,
        `str` INTEGER NOT NULL,
        `agi` INTEGER NOT NULL,
        `dex` INTEGER NOT NULL,
        `int` INTEGER NOT NULL
    );
    COMMIT;
";

const BOOTSTRAP_DATA: &str = "
    BEGIN;
    COMMIT;
";

pub fn create_database(
    conn: &PooledConnection<SqliteConnectionManager>,
) -> Result<(), rusqlite::Error> {
    conn.execute_batch(CREATE_DATABASE)?;
    conn.execute_batch(BOOTSTRAP_DATA)?;

    Ok(())
}
