use itertools::Itertools;
use crate::rpg::{classes::Class, stats::Stats};
use r2d2::PooledConnection;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{Row, Result};

#[derive(Clone, Debug)]
pub struct Character {
    pub uid: u64,
    pub xp: i32,
    pub class: Class,
    pub stats: Stats,
}

const GET_CHARACTER: &str = "
    SELECT * FROM `characters`
    WHERE `uid` = ?;
";

const CREATE_CHARACTER: &str = "
    INSERT OR IGNORE INTO `characters`
        ( `uid`, `class`, `str`, `agi`, `dex`, `int` )
    VALUES
        (?, ?, ?, ?, ?, ?);
";

const CLASS_CHANGE: &str = "
    UPDATE `characters`
    SET `class` = ?, `str` = ?, `agi` = ?, `dex` = ?, `int` = ?
    WHERE `uid` = ?;
";

fn map_character_row(row: &Row) -> Result<Character> {
    let class: i32 = row.get(1)?;

    let stats = Stats {
        hp: row.get(4)?,
        max_hp: row.get(5)?,
        mana: row.get(6)?,
        max_mana: row.get(7)?,
        str: row.get(8)?,
        agi: row.get(9)?,
        dex: row.get(10)?,
        int: row.get(11)?,
    };

    // TODO: get activity
    // let activity_id = row.get(2)?;

    let char = Character {
        uid: row.get(0)?,
        xp: row.get(3)?,
        class: class.try_into().unwrap(),
        stats,
    };

    Ok(char)
}

pub fn get_character(
    conn: &PooledConnection<SqliteConnectionManager>,
    uid: u64,
) -> Result<Character> {
    conn.query_row(GET_CHARACTER, (&uid,), map_character_row)
}

pub fn get_characters(
    conn: &PooledConnection<SqliteConnectionManager>,
) -> Vec<Result<Character>> {
    let mut query = conn.prepare("SELECT * FROM `characters`").unwrap();
    query.query_map([], map_character_row).unwrap().collect_vec()
}

pub fn create_character(
    conn: &PooledConnection<SqliteConnectionManager>,
    uid: u64,
) -> Result<Character, String> {
    let new_character = Character {
        uid,
        xp: 0,
        class: Class::Novice,
        stats: Stats::default(),
    };

    let result = get_character(conn, uid);

    if result.is_ok() {
        return Err("character already exists".to_string());
    }

    if let Err(e) = result {
        match e {
            rusqlite::Error::QueryReturnedNoRows => {}
            _ => {
                return Err(format!("error checking for existing character: {}", e));
            }
        }
    }

    let class: i32 = new_character.class.into();

    let create = conn.execute(
        CREATE_CHARACTER,
        (
            &new_character.uid,
            &class,
            &new_character.stats.str,
            &new_character.stats.agi,
            &new_character.stats.dex,
            &new_character.stats.int,
        )
    );

    if let Err(e) = create {
        return Err(format!("error creating character: {}", e));
    }

    Ok(new_character)
}

pub fn class_change(
    conn: &PooledConnection<SqliteConnectionManager>,
    uid: u64,
    class: Class,
) -> Result<(), String> {
    let character = get_character(conn, uid).unwrap();
    let stats = Stats::class_upgrade(class, character.stats);
    let class: i32 = class.into();
    let update = conn.execute(
        CLASS_CHANGE,
        (
            &class,
            &stats.str,
            &stats.agi,
            &stats.dex,
            &stats.int,
            &uid
        )
    );

    match update {
        Ok(1) => Ok(()),
        Ok(n) => Err(format!("{n} rows affected (more than one!)")),
        Err(e) => Err(format!("error class changing character: {}", e)),
    }
}