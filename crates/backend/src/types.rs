use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;

pub struct DbPoolContainer;
pub type DbPool = Pool<SqliteConnectionManager>;
pub struct ChainContainer;

pub enum Preposition {
    In,
    At,
}
