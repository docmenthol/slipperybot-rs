use crate::rest::meteo::forecast::{Forecast, ForecastResponse};
use reqwest::Client;

mod endpoints;
pub mod weather;
pub mod forecast;
pub mod hourly;

pub struct OpenMeteoClient {
    client: Client,
}

impl OpenMeteoClient {
    pub fn new() -> Self {
        let client = Client::builder().build().unwrap();
        Self { client }
    }

    pub async fn get_forecast(
        &self,
        latitude: String,
        longitude: String
    ) -> Result<Forecast, reqwest::Error> {
        let result = self
            .client
            .get(format!(
                "{}&latitude={}&longitude={}",
                endpoints::FORECAST,
                latitude,
                longitude
            ))
            .send()
            .await?;

        let response: ForecastResponse = result
            .json()
            .await?;

        Ok(Forecast::from_forecast_response(response))
    }
}

impl Default for OpenMeteoClient {
    fn default() -> Self {
        Self::new()
    }
}
