pub const FORECAST: &str =
    "https://api.open-meteo.com/v1/forecast?hourly=temperature_2m,precipitation_probability,rain,weather_code&temperature_unit=fahrenheit&wind_speed_unit=mph&precipitation_unit=inch&timeformat=unixtime&timezone=GMT&forecast_days=1";
