use std::fmt;

#[derive(Clone, Debug)]
pub enum Weather {
    ClearSky = 0,
    PartlySunny = 1,
    PartlyCloudy = 2,
    Overcast = 3,

    Fog = 45,
    DepositingRimeFog = 48,

    LightDrizzle = 51,
    ModerateDrizzle = 53,
    DenseDrizzle = 55,

    LightFreezingDrizzle = 56,
    DenseFreezingDrizzle = 57,

    LightRain = 61,
    ModerateRain = 63,
    HeavyRain = 65,

    LightFreezingRain = 66,
    HeavyFreezingRain = 67,

    SlightSnowfall = 71,
    ModerateSnowfall = 73,
    HeavySnowfall = 75,

    SnowGrains = 77,

    SlightRainShowers = 80,
    ModerateRainShowers = 81,
    ViolentRainShowers = 82,

    SlightSnowShowers = 85,
    HeavySnowShowers = 86,

    Thunderstorm = 95,
    ThunderstormWithSlightHail = 96,
    ThunderstormWithHeavyHail = 99,
}

impl TryFrom<i8> for Weather {
    type Error = String;

    fn try_from(weather_code: i8) -> Result<Self, Self::Error> {
        match weather_code {
            0 => Ok(Weather::ClearSky),
            1 => Ok(Weather::PartlySunny),
            2 => Ok(Weather::PartlyCloudy),
            3 => Ok(Weather::Overcast),

            45 => Ok(Weather::Fog),
            48 => Ok(Weather::DepositingRimeFog),

            51 => Ok(Weather::LightDrizzle),
            53 => Ok(Weather::ModerateDrizzle),
            55 => Ok(Weather::DenseDrizzle),

            56 => Ok(Weather::LightFreezingDrizzle),
            57 => Ok(Weather::DenseFreezingDrizzle),

            61 => Ok(Weather::LightRain),
            63 => Ok(Weather::ModerateRain),
            65 => Ok(Weather::HeavyRain),

            66 => Ok(Weather::LightFreezingRain),
            67 => Ok(Weather::HeavyFreezingRain),

            71 => Ok(Weather::SlightSnowfall),
            73 => Ok(Weather::ModerateSnowfall),
            75 => Ok(Weather::HeavySnowfall),

            77 => Ok(Weather::SnowGrains),

            80 => Ok(Weather::SlightRainShowers),
            81 => Ok(Weather::ModerateRainShowers),
            82 => Ok(Weather::ViolentRainShowers),

            85 => Ok(Weather::SlightSnowShowers),
            86 => Ok(Weather::HeavySnowShowers),

            95 => Ok(Weather::Thunderstorm),
            96 => Ok(Weather::ThunderstormWithSlightHail),
            99 => Ok(Weather::ThunderstormWithHeavyHail),

            _ => Err(format!("unknown weather code: {}", weather_code)),
        }
    }
}

impl fmt::Display for Weather {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let weather = match self {
            Weather::ClearSky => "clear sky",
            Weather::PartlySunny => "partly sunny",
            Weather::PartlyCloudy => "partly cloudy",
            Weather::Overcast => "overcast",

            Weather::Fog => "fog",
            Weather::DepositingRimeFog => "depositing rime fog",

            Weather::LightDrizzle => "light drizzle",
            Weather::ModerateDrizzle => "moderate drizzle",
            Weather::DenseDrizzle => "dense drizzle",

            Weather::LightFreezingDrizzle => "light freezing drizzle",
            Weather::DenseFreezingDrizzle => "dense freezing drizzle",

            Weather::LightRain => "light rain",
            Weather::ModerateRain => "moderate rain",
            Weather::HeavyRain => "heavy rain",

            Weather::LightFreezingRain => "light freezing rain",
            Weather::HeavyFreezingRain => "heavy freezing rain",

            Weather::SlightSnowfall => "slight snowfall",
            Weather::ModerateSnowfall => "moderate snowfall",
            Weather::HeavySnowfall => "heavy snowfall",

            Weather::SnowGrains => "snow grains",

            Weather::SlightRainShowers => "slight rain showers",
            Weather::ModerateRainShowers => "moderate rain showers",
            Weather::ViolentRainShowers => "violent rain showers",

            Weather::SlightSnowShowers => "slight snow showers",
            Weather::HeavySnowShowers => "heavy snow showers",

            Weather::Thunderstorm => "thunderstorm",
            Weather::ThunderstormWithSlightHail => "thunderstorm with slight hail",
            Weather::ThunderstormWithHeavyHail => "thunderstorm with heavy hail",
        };
        write!(f, "{}", weather)
    }
}

impl Weather {
    pub fn to_emoji(self) -> &'static str {
        match self {
            Weather::ClearSky => "☀️",
            Weather::PartlySunny => "🌤️",
            Weather::PartlyCloudy => "⛅",
            Weather::Overcast => "🌥️",

            Weather::Fog => "🌫️",
            Weather::DepositingRimeFog => "🌫️❄️",

            Weather::LightDrizzle => "🌦️",
            Weather::ModerateDrizzle => "🌦️🌦️",
            Weather::DenseDrizzle => "🌦️🌦️🌦️",

            Weather::LightFreezingDrizzle => "🌦️❄️",
            Weather::DenseFreezingDrizzle => "🌦️🌦️❄️",

            Weather::LightRain => "🌧️",
            Weather::ModerateRain => "🌧️🌧️",
            Weather::HeavyRain => "🌧️🌧️🌧️",

            Weather::LightFreezingRain => "🌧️❄️",
            Weather::HeavyFreezingRain => "🌧️🌧️❄️",

            Weather::SlightSnowfall => "❄️",
            Weather::ModerateSnowfall => "❄️❄️",
            Weather::HeavySnowfall => "❄️❄️❄️",

            Weather::SnowGrains => "❄️",

            Weather::SlightRainShowers => "🌧️",
            Weather::ModerateRainShowers => "🌧️🌧️",
            Weather::ViolentRainShowers => "🌧️🌧️🌧️",

            Weather::SlightSnowShowers => "❄️🌧️",
            Weather::HeavySnowShowers => "❄️🌧️🌧️",

            Weather::Thunderstorm => "⛈️",
            Weather::ThunderstormWithSlightHail => "⛈️🧊",
            Weather::ThunderstormWithHeavyHail => "⛈️🧊🧊",
        }
    }
}
