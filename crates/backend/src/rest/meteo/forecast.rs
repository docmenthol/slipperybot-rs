use crate::rest::meteo::hourly::{HourlyForecast, HourlyResponse, HourlyUnits};
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ForecastResponse {
    pub latitude: f32,
    pub longitude: f32,
    pub generationtime_ms: f64,
    pub utc_offset_seconds: i16,
    pub timezone: String,
    pub timezone_abbreviation: String,
    pub elevation: f32,
    pub hourly_units: HourlyUnits,
    pub hourly: HourlyResponse,
}

#[derive(Debug)]
pub struct Forecast {
    pub latitude: f32,
    pub longitude: f32,
    pub generationtime_ms: f64,
    pub utc_offset_seconds: i16,
    pub timezone: String,
    pub timezone_abbreviation: String,
    pub elevation: f32,
    pub hourly_units: HourlyUnits,
    pub hourly: Vec<HourlyForecast>,
}

impl Forecast {
    pub fn from_forecast_response(fr: ForecastResponse) -> Self {
        let hourly = itertools::izip!(
            fr.hourly.time,
            fr.hourly.temperature_2m,
            fr.hourly.precipitation_probability,
            fr.hourly.rain,
            fr.hourly.weather_code
        )
        .map(
            |(time, temperature_2m, precipitation_probability, rain, wmo_code)| {
                let datetime: DateTime<Utc> = DateTime::from_timestamp(time, 0).unwrap();
                let weather = wmo_code.try_into().unwrap();

                HourlyForecast {
                    datetime,
                    temperature_2m,
                    precipitation_probability,
                    rain,
                    weather,
                }
            },
        )
        .collect::<Vec<HourlyForecast>>();

        Self {
            latitude: fr.latitude,
            longitude: fr.longitude,
            generationtime_ms: fr.generationtime_ms,
            utc_offset_seconds: fr.utc_offset_seconds,
            timezone: fr.timezone,
            timezone_abbreviation: fr.timezone_abbreviation,
            elevation: fr.elevation,
            hourly_units: fr.hourly_units,
            hourly,
        }
    }
}
