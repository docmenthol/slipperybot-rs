use crate::rest::meteo::weather::Weather;
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct HourlyUnits {
    pub time: String,
    pub temperature_2m: String,
    pub precipitation_probability: String,
    pub rain: String,
    pub weather_code: String,
}

#[derive(Deserialize, Debug)]
pub struct HourlyResponse {
    pub time: Vec<i64>,
    pub temperature_2m: Vec<f32>,
    pub precipitation_probability: Vec<u8>,
    pub rain: Vec<f32>,
    pub weather_code: Vec<i8>,
}

#[derive(Clone, Debug)]
pub struct HourlyForecast {
    pub datetime: DateTime<Utc>,
    pub temperature_2m: f32,
    pub precipitation_probability: u8,
    pub rain: f32,
    pub weather: Weather,
}
