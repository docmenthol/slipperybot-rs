use serde::Deserialize;
use reqwest::{Client, Result};

mod endpoints;

#[derive(Debug)]
pub struct Location {
    pub name: String,
    pub latitude: String,
    pub longitude: String
}

#[derive(Deserialize, Debug)]
pub struct Place {
    #[serde(alias = "place name")]
    pub place_name: String,
    pub longitude: String,
    pub state: String,
    #[serde(alias = "state abbreviation")]
    pub state_abbreviation: String,
    pub latitude: String,
}

#[derive(Deserialize, Debug)]
pub struct LocationResponse {
    #[serde(alias = "post code")]
    post_code: String,
    country: String,
    #[serde(alias = "country abbreviation")]
    country_abbreviation: String,
    places: Vec<Place>,
}

pub struct ZippopotamusClient {
    client: Client,
}

impl ZippopotamusClient {
    pub fn new() -> Self {
        let client = Client::builder().build().unwrap();
        Self { client }
    }

    pub async fn get_coords(&self, location: String) -> Result<Location> {
        let result = self
            .client
            .get(format!(
                "{}/{}",
                endpoints::ENDPOINT,
                location,
            ))
            .send()
            .await?;

        let response: LocationResponse = result
            .json()
            .await?;

        let place = response.places.first().unwrap();
        Ok(Location {
            name: format!("{}, {}", place.place_name.clone(), place.state_abbreviation.clone()),
            latitude: place.latitude.clone(),
            longitude: place.longitude.clone()
        })
    }
}

impl Default for ZippopotamusClient {
    fn default() -> Self {
        Self::new()
    }
}