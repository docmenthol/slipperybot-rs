use reqwest::{Response, Error};
use dotenvy::dotenv;
use reqwest::{Client, RequestBuilder};
use serde::Serialize;
use std::env;

mod endpoints;

#[derive(Serialize)]
pub struct Area {
    pub area_id: String,
}

#[derive(Serialize)]
pub struct Entity {
    pub entity_id: String,
}

#[derive(Debug)]
pub struct HomeAssistantClient {
    token: String,
    base_url: String,
    client: Client,
}

impl HomeAssistantClient {
    pub fn new() -> Self {
        dotenv().expect(".env file not found");
        let token = env::var("HA_TOKEN").expect("HA_TOKEN env variable not set");
        let base_url = env::var("HA_URL").expect("HA_URL env variable not set");
        let client = Client::builder().build().unwrap();

        Self {
            token,
            base_url,
            client,
        }
    }

    fn get(&self, endpoint: &str) -> RequestBuilder {
        self.client
            .get(format!("{}/api/{}", self.base_url, endpoint))
            .bearer_auth(self.token.clone())
    }

    fn post<T: Serialize>(&self, endpoint: &str, body: T) -> RequestBuilder {
        self.client
            .post(format!("{}/api/{}", self.base_url, endpoint))
            .json(&body)
            .bearer_auth(self.token.clone())
    }

    async fn toggle<T: Serialize>(&self, body: T) -> Result<Response, Error> {
        self.post(endpoints::TOGGLE, body).send().await
    }

    async fn turn_on<T: Serialize>(&self, body: T) -> Result<Response, Error> {
        self.post(endpoints::TURN_ON, body).send().await
    }

    async fn turn_off<T: Serialize>(&self, body: T) -> Result<Response, Error> {
        self.post(endpoints::TURN_OFF, body).send().await
    }

    pub async fn toggle_area(&self, area_id: String) -> Result<Response, Error> {
        self.toggle(Area { area_id }).await
    }

    pub async fn turn_area_on(&self, area_id: String) -> Result<Response, Error> {
        self.turn_on(Area { area_id }).await
    }

    pub async fn turn_area_off(&self, area_id: String) -> Result<Response, Error> {
        self.turn_off(Area { area_id }).await
    }

    pub async fn toggle_entity(&self, entity_id: String) -> Result<Response, Error> {
        self.toggle(Entity { entity_id }).await
    }

    pub async fn turn_entity_on(&self, entity_id: String) -> Result<Response, Error> {
        self.turn_on(Entity { entity_id }).await
    }

    pub async fn turn_entity_off(&self, entity_id: String) -> Result<Response, Error> {
        self.turn_off(Entity { entity_id }).await
    }
}

impl Default for HomeAssistantClient {
    fn default() -> Self {
        Self::new()
    }
}