pub const TOGGLE: &str = "services/homeassistant/toggle";
pub const TURN_ON: &str = "services/homeassistant/turn_on";
pub const TURN_OFF: &str = "services/homeassistant/turn_off";
