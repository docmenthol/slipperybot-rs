use std::path::PathBuf;
use std::process::Command;

// ffmpeg -i input_file \
// 	-y \
// 	-strict unofficial \
// 	-c:v mpeg1video \
// 	-vf scale=240:-1:flags=neighbor \
// 	-r 12 \
// 	-c:a mp2 \
// 	-af volume=50 \
// 	-ar 16000 \
// 	-b:a 16k \
// 	-b:v 128k \
// 	output_file.avi

pub fn crush_video(input_file: PathBuf) -> PathBuf {
    let mut output_file = input_file.clone();
    output_file.set_extension("avi");

    let input_filename = input_file
        .to_str()
        .expect("failed to convert input filename");

    let output_filename = output_file
        .to_str()
        .expect("failed to convert output filename");

    let mut crush_cmd = Command::new("./data/ffmpeg/bin/ffmpeg");
    crush_cmd
        .args(["-i", input_filename])
        .arg("-y")
        .args(["-strict", "unofficial"])
        .args(["-c:v", "mpeg1video"])
        .args(["-vf", "scale=240x136:flags=neighbor"])
        .args(["-r", "12"])
        .args(["-c:a", "mp2"])
        .args(["-af", "volume=50"])
        .args(["-ar", "16000"])
        .args(["-b:a", "16k"])
        .args(["-b:v", "128k"])
        .arg(output_filename);

    crush_cmd
        .output()
        .expect("failed to execute ./assets/ffmpeg/bin/ffmpeg");

    output_file
}

// ffmpeg -i input_file \
// 	-y \
// 	-c:v libx264 \
// 	-vf scale=1280x720:flags=neighbor \
// 	-pix_fmt yuv420p \
// 	-crf 45 \
// 	-c:a libmp3lame \
// 	-af volume=0.8 \
// 	output_file.mp4

pub fn expand_video(input_file: PathBuf) -> PathBuf {
    let mut output_file = input_file.clone();

    if let Some(stem) = output_file.file_stem().unwrap().to_str() {
        let crushed_file_name = format!("{}-crushed.mp4", stem);
        output_file.set_file_name(crushed_file_name);

        let input_filename = input_file
            .to_str()
            .expect("failed to convert input filename");

        let output_filename = output_file
            .to_str()
            .expect("failed to convert output filename");

        let mut expand_cmd = Command::new("./data/ffmpeg/bin/ffmpeg");
        expand_cmd
            .args(["-i", input_filename])
            .arg("-y")
            .args(["-c:v", "libx264"])
            .args(["-vf", "scale=1280x720:flags=neighbor"])
            .args(["-pix_fmt", "yuv420p"])
            .args(["-crf", "45"])
            .args(["-c:a", "aac"])
            .args(["-ac", "2"])
            .args(["-af", "volume=0.8"])
            .arg(output_filename);

        expand_cmd
            .output()
            .expect("failed to execute ./data/ffmpeg/bin/ffmpeg");
    }

    output_file
}
