#![feature(trivial_bounds)]

use std::env;
use diesel::{SqliteConnection, r2d2::{Pool, ConnectionManager}};
use dotenvy::dotenv;

mod schema;
pub mod rpg;

pub fn get_pool() -> Pool<ConnectionManager<SqliteConnection>> {
    dotenv()
        .expect(".env file not found");

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL env variable not set");

    let manager = ConnectionManager::<SqliteConnection>::new(database_url);

    Pool::builder()
        .test_on_check_out(true)
        .build(manager)
        .expect("failed to build connection pool")
}