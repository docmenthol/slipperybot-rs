use crate::rpg::classes::Class;

#[derive(Clone, Copy, Debug)]
pub struct Stats {
    pub hp: u16,
    pub max_hp: u16,
    pub mana: u16,
    pub max_mana: u16,
    pub str: u16,
    pub agi: u16,
    pub dex: u16,
    pub int: u16,
}

impl Default for Stats {
    fn default() -> Self {
        Self {
            hp: 10,
            max_hp: 10,
            mana: 10,
            max_mana: 10,
            str: 3,
            agi: 3,
            dex: 3,
            int: 3,
        }
    }
}

impl Stats {
    pub fn new(hp: u16, max_hp: u16, mana: u16, max_mana: u16, str: u16, agi: u16, dex: u16, int: u16) -> Self {
        Self { hp, max_hp, mana, max_mana, str, agi, dex, int }
    }

    pub fn new_from(old_stats: Stats) -> Self {
        Self::new(old_stats.hp, old_stats.max_hp, old_stats.mana, old_stats.max_mana, old_stats.str, old_stats.agi, old_stats.dex, old_stats.int)
    }

    pub fn class_change(class: Class, old_stats: Stats) -> Self {
        let mut stats = Stats::new_from(old_stats);

        match class {
            Class::Novice => {}

            Class::Fighter => {
                stats.str += 7;
                stats.agi += 1;
                stats.dex += 3;
            }

            Class::Rogue => {
                stats.str += 3;
                stats.agi += 7;
                stats.dex += 1;
            }

            Class::Archer => {
                stats.agi += 3;
                stats.dex += 7;
                stats.int += 1;
            }

            Class::Mage => {
                stats.agi += 1;
                stats.dex += 3;
                stats.int += 7;
            }
        }

        stats
    }
}
