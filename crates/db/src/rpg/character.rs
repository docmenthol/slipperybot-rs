use diesel::{Selectable, SqliteConnection, query_dsl::filter_dsl::FindDsl, FromSqlRow, QueryDsl, AsExpression};
use crate::rpg::{classes::Class, stats::Stats, equipment::Equipment, item::Item};
use crate::schema::characters::dsl::characters;

#[derive(FromSqlRow, Selectable, AsExpression)]
#[diesel(table_name = crate::schema::characters)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
struct CharacterRow {
    pub uid: u64,
    pub class: u8,
    pub activity: String,
    pub xp: u32,
    pub hp: u16,
    pub max_hp: u16,
    pub mana: u16,
    pub max_mana: u16,
    pub str: u16,
    pub agi: u16,
    pub dex: u16,
    pub int: u16,
}

#[derive(Debug)]
pub struct Character {
    pub uid: u64,
    pub xp: u32,
    pub class: Class,
    pub stats: Stats,
    pub equipment: Equipment,
    pub inventory: Vec<Item>,
}

impl Character {
    pub fn from_row(row: CharacterRow) -> Character {
        Character {
            uid: row.uid,
            xp: row.xp,
            class: row.class.try_into().unwrap(),
            stats: Stats::new(row.hp, row.max_hp, row.mana, row.max_mana, row.str, row.agi, row.dex, row.int),
            equipment: Equipment::default(),
            inventory: vec![]
        }
    }
}

pub fn character_exists(conn: &mut SqliteConnection, uid: u64) -> bool {
    let result = characters.count().filter(uid.eq(&uid)).get_result(conn)?;
    result > 0
}

pub fn get_character(conn: &mut SqliteConnection, uid: u64) -> Option<Character> {
    let result = characters
        .find(uid)
        .filter(uid.eq(&uid))
        .first::<CharacterRow>(conn).ok();

    match result {
        Some(row) => Some(Character::from_row(row)),
        None => None
    }
}
