use diesel::Selectable;
use crate::rpg::item::Item;

#[derive(Selectable, Debug)]
pub struct Equipment {
    head: Item,
    body: Item,
    legs: Item,
    feet: Item,
    weapon: Item,
    amulet: Item,
    ring_a: Item,
    ring_b: Item,
}

impl Default for Equipment {
    fn default() -> Self {
        Self {
            head: Item::new("helmet"),
            body: Item::new("tunic"),
            legs: Item::new("pants"),
            feet: Item::new("boots"),
            weapon: Item::new("sword"),
            amulet: Item::new("amulet"),
            ring_a: Item::new("copper ring"),
            ring_b: Item::new("iron ring"),
        }
    }
}