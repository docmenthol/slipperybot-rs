use diesel::{
    AsExpression,
    serialize,
    serialize::ToSql,
    sql_types::Integer,
    sqlite::Sqlite
};

#[derive(Clone, Copy, Debug, AsExpression)]
#[diesel(sql_type = Integer)]
pub enum Class {
    Novice,
    Fighter,
    Rogue,
    Archer,
    Mage,
}

impl From<Class> for u8 {
    fn from(class: Class) -> u8 {
        match class {
            Class::Novice => 0,
            Class::Fighter => 1,
            Class::Rogue => 2,
            Class::Archer => 3,
            Class::Mage => 4,
        }
    }
}

impl ToSql<Integer, Sqlite> for Class {
    fn to_sql<'b>(&'b self, out: &mut serialize::Output<'b, '_, Sqlite>) -> serialize::Result {
        <i16 as ToSql<Integer, Sqlite>>::to_sql(&(*self as i16), out)
    }
}

impl From<Class> for String {
    fn from(class: Class) -> String {
        match class {
            Class::Novice => "novice".to_string(),
            Class::Fighter => "fighter".to_string(),
            Class::Rogue => "rogue".to_string(),
            Class::Archer => "archer".to_string(),
            Class::Mage => "mage".to_string(),
        }
    }
}

impl TryFrom<u8> for Class {
    type Error = ();

    fn try_from(class_id: u8) -> Result<Self, Self::Error> {
        match class_id {
            0 => Ok(Class::Novice),
            1 => Ok(Class::Fighter),
            2 => Ok(Class::Rogue),
            3 => Ok(Class::Archer),
            4 => Ok(Class::Mage),
            _ => Err(()),
        }
    }
}
