// @generated automatically by Diesel CLI.

diesel::table! {
    characters (uid) {
        uid -> BigInt,
        class -> Integer,
        activity -> Text,
        xp -> Integer,
        hp -> Integer,
        max_hp -> Integer,
        mana -> Integer,
        max_mana -> Integer,
        str -> Integer,
        agi -> Integer,
        dex -> Integer,
        int -> Integer,
    }
}
