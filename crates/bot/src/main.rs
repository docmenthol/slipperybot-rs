#![feature(trivial_bounds)]

mod handler;
mod background;

use crate::background::Background;
use backend::{config::Config, state::State};
use handler::event_handler;
use rustkov::prelude::*;
use std::{env, path::{Path, PathBuf}};
use dotenvy::dotenv;
use poise::serenity_prelude as serenity;
use clap::Parser;
use log::{error,  info, trace};
use poise::serenity_prelude::ChannelId;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[arg(short, long, value_name = "FILE")]
    config_file: Option<PathBuf>,
    #[arg(short = 'm', long, value_name = "CHAIN")]
    chain: Option<PathBuf>
}

#[tokio::main]
async fn main() {
    env_logger::Builder::new()
        .filter(Some("slipperybot"), log::LevelFilter::Trace)
        .filter(Some("backend"), log::LevelFilter::Trace)
        .filter(Some("commands"), log::LevelFilter::Trace)
        .filter(Some("db"), log::LevelFilter::Trace)
        .init();

    info!(".: slipperybot :.");

    dotenv()
        .expect(".env file not found");

    let token = env::var("DISCORD_TOKEN")
        .expect("DISCORD_TOKEN env variable not set");
    let intents =
        serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT;

    let args = Cli::parse();

    trace!("parsed args");
    
    let config_file = match args.config_file {
        Some(c) => {
            if !c.exists() {
                error!("config file not found: {:?}", c);
                return;
            }

            c.as_path().to_path_buf()
        }
        None => {
            if !Path::new("data/Config.toml").exists() {
                error!("default config file not found.");
                return;
            }

            Path::new("data/Config.toml").to_path_buf()
        }
    };

    let config = Config::new(config_file);

    trace!("config init");

    let chain_file = match args.chain {
        Some(c) => {
            if !c.exists() {
                error!("chain not found: {:?}", c);
                return;
            }

            c.as_path().to_path_buf()
        }
        None => {
            if !Path::new("data/Chain.bin").exists() {
                info!("no chain found. creating.");

                let chain_config = BrainConfig::from_file("data/Chain.toml")
                    .expect("Could not load brain config.");
                let chain = Brain::new()
                    .config(chain_config)
                    .expect("Could not apply config.")
                    .from_dataset("data/training_data.txt")
                    .expect("Unable to train new chain!")
                    .get();
                chain.to_file("data/Chain.bin")
                    .expect("Unable to save new brain!");
            } else {
                trace!("using existing chain.");
            }

            Path::new("data/Chain.bin").to_path_buf()
        }
    };

    info!("loading saved chain");

    let mut chain = Brain::from_file(chain_file.to_str().unwrap())
        .expect("failed to load saved chain!");

    chain.config = BrainConfig::from_file("data/Chain.toml")
        .expect("could not load brain config.");

    let manager = SqliteConnectionManager::file("data/bot.db");
    let pool = Pool::new(manager).unwrap();

    trace!("sqlite pool init");

    let state = State::new(config.clone(), chain, pool.clone());

    trace!("state init");

    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![
                commands::admin::admin(),
                commands::ask::ask(),
                commands::ask::stats(),
                commands::dice::roll(),
                commands::help::help(),
                commands::hl::hev(),
                commands::hl::vox(),
                commands::hl::wordlist(),
                commands::reminders::reminder(),
                commands::rpg::character::character(),
                commands::rpg::character::view_cm(),
                commands::weather::weather(),
                commands::lights::tl(),
            ],
            command_check: Some(|ctx| {
                Box::pin(async move {
                    if ctx.command().qualified_name == "admin" {
                        let config = ctx.data().config.lock().await;
                        let admins = &config.server.admins;
                        Ok(admins.contains(&ctx.author().id.into()))
                    } else {
                        Ok(true)
                    }
                })
            }),
            event_handler: |ctx, event, framework, data| Box::pin(event_handler(ctx, event, framework, data)),
            prefix_options: poise::PrefixFrameworkOptions {
                prefix: Some("!".into()),
                ..Default::default()
            },
            ..Default::default()
        })
        .setup(|ctx, _ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                Ok(state)
            })
        })
        .build();

    trace!("framework init");

    let mut client = serenity::Client::builder(&token, intents)
        .framework(framework)
        .activity(serenity::ActivityData::custom("beeping and booping"))
        .await.unwrap();

    info!("load complete. starting client.");

    tokio::spawn(async move {
        let channel_id = ChannelId::new(config.server.channel);
        let background = Background::new(channel_id, pool);
        background.task().await;
    });

    client.start().await.unwrap()

    // tokio::join!(background.task(), client.start());
}