use std::env;
use poise::serenity_prelude::{ChannelId, CreateMessage, Http};
use r2d2_sqlite::SqliteConnectionManager;
use r2d2::Pool;
use backend::db::character;

pub struct Background {
    token: String,
    channel_id: ChannelId,
    pool: Pool<SqliteConnectionManager>
}

impl Background {
    pub fn new(channel_id: ChannelId, pool: Pool<SqliteConnectionManager>) -> Self {
        Self {
            token: env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN env variable not set"),
            channel_id,
            pool
        }
    }

    pub async fn task(&self) {
        // let http = &Http::new(self.token.as_str());
        // let conn = self.pool.get().unwrap();
        let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
        loop {
            interval.tick().await;

            // self.channel_id.send_message(http, CreateMessage::new().content("tick!")).await.unwrap();
            // let chars = character::get_characters(&conn);
        }
    }
}