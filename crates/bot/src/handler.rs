use commands::types::Error;
use backend::state::State;
use poise::serenity_prelude as serenity;
use log::{trace, info};

pub async fn event_handler(
	_ctx: &serenity::Context,
    event: &serenity::FullEvent,
    framework: poise::FrameworkContext<'_, State, Error>,
    _state: &State,
) -> Result<(), Error> {
	match event {
		serenity::FullEvent::Ready { data_about_bot: _ } => {
			info!("slipperybot ready.");
		},
		serenity::FullEvent::Message { new_message } => {
			if let Some(_i) = &new_message.interaction {
				return Ok(())
			}

			if new_message.content.starts_with("!") {
				return Ok(())
			}

			if new_message.author.bot {
				return Ok(())
			}

			let mut chain = framework.user_data().await.chain.lock().await;
			chain.ingest(&new_message.content);
			trace!("[brain] learned \"{}\"", new_message.content);
		},
		_ => {}
	}
	
	Ok(())
}
