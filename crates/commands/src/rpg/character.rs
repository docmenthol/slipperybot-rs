use crate::types::{Context, Error};
use backend::{
    db::character,
    rpg::{level, classes::Class},
};
use poise::{
    serenity_prelude::{CreateActionRow, CreateButton, CreateEmbed, User},
    CreateReply
};

async fn view_character_callback(ctx: Context<'_>) -> Result<(), Error> {
    let conn = ctx.data().pool.get()?;
    let result = character::get_character(&conn, ctx.author().id.into());

    if let Ok(character) = result {
        let class_name: String = character.class.into();
        let level = level::level_for_xp(character.xp);
        let embed = CreateEmbed::default()
            .title(format!("{} the {}", ctx.author().name, class_name))
            .field(
                "Level",
                level.to_string(),
                true
            )
            .field(
                "XP",
                format!("{}/{}", character.xp, level::xp_for_level(level + 1)),
                true
            );
        ctx.send(
            CreateReply::default()
                .embed(embed)
                .ephemeral(true)
        ).await?;
    } else {
        ctx.send(
            CreateReply::default()
                .content("you haven't started your character yet.")
                .ephemeral(true)
        ).await?;
    }

    Ok(())
}

#[poise::command(prefix_command, slash_command, subcommands("create", "view"), subcommand_required)]
pub async fn character(
    _ctx: Context<'_>
) -> Result<(), Error> {
    Ok(())
}

#[poise::command(prefix_command, slash_command, reuse_response)]
pub async fn create(
    ctx: Context<'_>,
) -> Result<(), Error> {
    let conn = ctx.data().pool.get()?;
    let uid = ctx.author().id;
    let r = character::get_character(&conn, uid.into());
    if r.is_ok() {
        ctx.send(
            CreateReply::default()
                .content("you already have a character.")
                .ephemeral(true)
        ).await?;
        return Ok(());
    }

    let result = character::create_character(&conn, uid.into());

    let reply = match result {
        Ok(_) => "you are now a novice.".to_string(),
        Err(e) => e.to_string(),
    };

    ctx.send(
        CreateReply::default()
            .content(reply)
            .reply(true)
            .ephemeral(true)
    ).await?;

    Ok(())
}

#[poise::command(prefix_command, slash_command, reuse_response)]
pub async fn classchange(
    ctx: Context<'_>,
) -> Result<(), Error> {
    let conn = ctx.data().pool.get()?;
    let uid = ctx.author().id;
    let r = character::get_character(&conn, uid.into());
    if r.is_err() {
        ctx.send(
            CreateReply::default()
                .content("you already have a character.")
                .ephemeral(true)
        ).await?;
        return Ok(());
    }

    let fighter_bid = format!("fighter-{}", ctx.id());
    let fighter_button = CreateButton::new(fighter_bid.clone())
        .label("fighter");

    let rogue_bid = format!("rogue-{}", ctx.id());
    let rogue_button = CreateButton::new(rogue_bid.clone())
        .label("rogue");

    let archer_bid = format!("archer-{}", ctx.id());
    let archer_button = CreateButton::new(archer_bid.clone())
        .label("archer");

    let mage_bid = format!("mage-{}", ctx.id());
    let mage_button = CreateButton::new(mage_bid.clone())
        .label("mage");

    let class_buttons = CreateActionRow::Buttons(vec![
        fighter_button.clone(),
        rogue_button.clone(),
        archer_button.clone(),
        mage_button.clone(),
    ]);

    let reply = CreateReply::default()
        .content("Choose your class:")
        .components(vec![class_buttons])
        .ephemeral(true);

    let handle = ctx.send(reply).await?;

    let message = match handle.clone().into_message().await {
        Ok(message) => message,
        Err(_) => return Ok(())
    };

    match message
        .await_component_interaction(&ctx.serenity_context().shard)
        .timeout(std::time::Duration::from_secs(60 * 2))
        .author_id(ctx.author().id)
        .custom_ids(vec![fighter_bid.clone(), rogue_bid.clone(), archer_bid.clone(), mage_bid.clone()])
        .await
    {
        Some(val) => {
            let (tag, class) = if val.data.custom_id.eq(&fighter_bid) {
                ("a fighter".to_string(), Some(Class::Fighter))
            } else if val.data.custom_id.eq(&rogue_bid) {
                ("a rogue".to_string(), Some(Class::Rogue))
            } else if val.data.custom_id.eq(&archer_bid) {
                ("an archer".to_string(), Some(Class::Archer))
            } else if val.data.custom_id.eq(&mage_bid) {
                ("a mage".to_string(), Some(Class::Mage))
            } else {
                handle.edit(
                    ctx,
                    CreateReply::default()
                        .content("how did you get here?")
                        .components(vec![])
                        .ephemeral(true)
                ).await?;
                return Ok(());
            };

            if let Some(cls) = class {
                let result = character::class_change(&conn, uid.into(), cls);

                let reply = match result {
                    Ok(_) => format!("you are now {}.", tag),
                    Err(e) => e.to_string(),
                };

                handle.edit(
                    ctx,
                    CreateReply::default()
                        .content(reply)
                        .components(vec![])
                        .ephemeral(true)
                ).await?;
            } else {
                handle.edit(
                    ctx,
                    CreateReply::default()
                        .content(tag)
                        .components(vec![])
                        .ephemeral(true)
                ).await?;
            }
        }
        None => {}
    };

    Ok(())
}

#[poise::command(prefix_command, slash_command)]
pub async fn view(
    ctx: Context<'_>,
) -> Result<(), Error> {
    view_character_callback(ctx).await
}

#[poise::command(context_menu_command = "View character")]
pub async fn view_cm(
    ctx: Context<'_>,
    #[description = "User that owns this character"]
    _user: User,
) -> Result<(), Error> {
    view_character_callback(ctx).await
}