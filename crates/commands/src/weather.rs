use crate::{get_default_activity, types::{Context, Error}};
use backend::{
    zipcodes::ZIPCODES,
    rest::{
        meteo::OpenMeteoClient,
        zippopotamus::ZippopotamusClient
    }
};
use poise::{
    CreateReply,
    Context::Application,
    serenity_prelude::{CreateEmbed, ActivityData}
};
use futures::{Stream, StreamExt};

async fn autocomplete_zipcode<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(ZIPCODES)
        .filter(move |zip| futures::future::ready(zip.starts_with(partial)))
        .map(|zip| format!("{:0<5}", zip.to_string()))
        .take(15)
}

#[poise::command(prefix_command, slash_command)]
pub async fn weather(
    ctx: Context<'_>,
    #[description = "zipcode"]
    #[autocomplete = "autocomplete_zipcode"]
    zipcode: String
) -> Result<(), Error> {
    if ZIPCODES.binary_search(&zipcode.as_str()).is_err() {
        ctx.send(ctx.reply_builder(CreateReply::default()).content("i don't know that zipcode")).await?;
        return Ok(())
    }

    if let Application(ctx) = ctx {
        let thinking = ActivityData::custom("putting on my meteorologist suit...");
        ctx.serenity_context.set_activity(Some(thinking));

        let om_client = OpenMeteoClient::new();
        let zip_client = ZippopotamusClient::new();

        let location = zip_client.get_coords(format!("us/{zipcode}")).await.unwrap();
        let forecast = om_client.get_forecast(location.latitude, location.longitude).await.unwrap();

        let hourly_iter = forecast.hourly.clone().into_iter();
        let fields = hourly_iter.map(|hourly| {
            let dt = format!("<t:{}:F>", hourly.datetime.timestamp());
            let hourly_forecast = format!(
                "temperature: {}{}\nweather: {} {}\nrain: {} {}, {}{} chance",
                hourly.temperature_2m,
                forecast.hourly_units.temperature_2m,
                hourly.weather.clone(),
                hourly.weather.clone().to_emoji(),
                hourly.rain,
                forecast.hourly_units.rain,
                hourly.precipitation_probability,
                forecast.hourly_units.precipitation_probability
            );
            (dt, hourly_forecast, true)
        }).collect::<Vec<(String, String, bool)>>();

        let config = ctx.data().config.lock().await;
        let reply = ctx.reply_builder(CreateReply::default())
            .embed(
                    CreateEmbed::new()
                        .title(format!("weather for {} ({})", location.name.to_lowercase(), zipcode))
                        .fields(fields)
                )
            .ephemeral(config.features.introverted);

        ctx.send(reply).await?;

        ctx.serenity_context.set_activity(Some(get_default_activity()));
    }

    Ok(())
}
