use crate::{get_default_activity, types::{Context, Error}};
use futures::{Stream, StreamExt};
use poise::CreateReply;
use poise::serenity_prelude::{ActivityData, RoleId};

async fn autocomplete_area_id<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(&["living_room"])
        .filter(move |name| futures::future::ready(name.starts_with(partial)))
        .map(|name| name.to_string())
}

#[poise::command(prefix_command, slash_command, reuse_response)]
pub async fn tl(
    ctx: Context<'_>,
    #[description = "Area to toggle"]
    #[autocomplete = "autocomplete_area_id"]
    area_id: String
) -> Result<(), Error> {
    let thinking = ActivityData::custom("announcing...");
    ctx.serenity_context().set_activity(Some(thinking));

    let config = ctx.data().config.lock().await;

    let member = ctx.author_member().await;

    if member.is_none() {
        ctx.send(
            CreateReply::default()
                .content("you're not allowed to toggle lights, nerd.")
                .components(vec![])
                .ephemeral(config.features.introverted)
        ).await?;
        return Ok(())
    }

    let member = member.unwrap();
    if !member.roles.contains(&RoleId::new(1288920033543585924)) {
        ctx.send(
            CreateReply::default()
                .content("you're not allowed to toggle lights, nerd.")
                .components(vec![])
                .ephemeral(config.features.introverted)
        ).await?;
        return Ok(())
    }

    let body = format!("toggling area {}...", area_id);
    let handle = ctx.send(
        CreateReply::default()
            .content(&body)
            .components(vec![])
            .ephemeral(config.features.introverted)
    ).await?;

    let ha_client = &ctx.data().ha_client;
    ha_client.toggle_area(area_id).await?;

    handle.edit(
        ctx,
        CreateReply::default()
            .content(format!("{} done!", body))
            .components(vec![])
            .ephemeral(config.features.introverted)
    ).await?;

    ctx.serenity_context().set_activity(Some(get_default_activity()));

    Ok(())
}