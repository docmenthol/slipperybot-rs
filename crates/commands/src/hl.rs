use std::fs;
use crate::{get_default_activity, types::{Context, Error}};
use futures::{Stream, StreamExt};
use poise::CreateReply;
use poise::serenity_prelude::{ActivityData, CreateAttachment};
use backend::audio;

async fn autocomplete_wordlist_name<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(&["hev", "vox", "grunt"])
        .filter(move |name| futures::future::ready(name.starts_with(partial)))
        .map(|name| name.to_string())
}

#[poise::command(slash_command)]
pub async fn hev(
    ctx: Context<'_>,
    #[description = "what to say"]
    what: String
) -> Result<(), Error> {
    let thinking = ActivityData::custom("suiting up...");
    ctx.serenity_context().set_activity(Some(thinking));

    // TODO
    let words: Vec<String> = what
        .replace(",", " _comma")
        .replace(".", " _period")
        .split(" ")
        .take(20)
        .map(|w| w.to_string())
        .collect();

    let output_file = audio::stitch_audio(audio::ClipSource::Hev, words, ctx.id().to_string());
    let attachment = CreateAttachment::path(output_file.clone()).await?;

    ctx.send(
        CreateReply::default()
            .attachment(attachment)
            .ephemeral(true)
    ).await?;

    fs::remove_file(output_file).unwrap();

    ctx.serenity_context().set_activity(Some(get_default_activity()));

    Ok(())
}

#[poise::command(slash_command)]
pub async fn vox(
    ctx: Context<'_>,
    #[description = "what to say"]
    _what: String
) -> Result<(), Error> {
    let thinking = ActivityData::custom("announcing...");
    ctx.serenity_context().set_activity(Some(thinking));

    // TODO

    ctx.serenity_context().set_activity(Some(get_default_activity()));

    Ok(())
}

#[poise::command(slash_command)]
pub async fn grunt(
    ctx: Context<'_>,
    #[description = "what to say"]
    _what: String
) -> Result<(), Error> {
    let thinking = ActivityData::custom("grunting...");
    ctx.serenity_context().set_activity(Some(thinking));

    // TODO

    ctx.serenity_context().set_activity(Some(get_default_activity()));

    Ok(())
}

#[poise::command(slash_command)]
pub async fn wordlist(
    ctx: Context<'_>,
    #[description = "Which wordlist"]
    #[autocomplete = "autocomplete_wordlist_name"]
    wordlist: Option<String>
) -> Result<(), Error> {
    if let Some(k) = wordlist {
        match k.as_str() {
            "hev" => {
                ctx.reply("hev wordlist.").await?;
            }
            "vox" => {
                ctx.reply("vox wordlist").await?;
            }
            "grunt" => {
                ctx.reply("grunt wordlist").await?;
            }
            _ => {
                ctx.reply(format!("sorry, i don't know about the {} wordlist.", k)).await?;
            }
        }
    } else {
        ctx.reply("wordlist must be \"hev\", \"vox\", or \"grunt\".").await?;
    }

    Ok(())
}
