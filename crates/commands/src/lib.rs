pub mod admin;
pub mod ask;
pub mod dice;
pub mod help;
pub mod hl;
pub mod reminders;
pub mod rpg;
pub mod types;
pub mod weather;
pub mod lights;

use poise::serenity_prelude::ActivityData;
pub fn get_default_activity() -> ActivityData {
    ActivityData::custom("beeping and booping")
}
