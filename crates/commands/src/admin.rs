use crate::types::{Context, Error};
use backend::utils;
use futures::{Stream, StreamExt};
use poise::CreateReply;

async fn autocomplete_bool<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(&["on", "off"])
        .filter(move |name| futures::future::ready(name.starts_with(partial)))
        .map(|name| name.to_string())
}

#[poise::command(slash_command, subcommands("introverted", "silent"), subcommand_required)]
pub async fn admin(
    _ctx: Context<'_>
) -> Result<(), Error> {
    Ok(())
}

#[poise::command(slash_command)]
pub async fn introverted(
    ctx: Context<'_>,
    #[description = "new value"]
    #[autocomplete = "autocomplete_bool"]
    new_value: Option<String>
) -> Result<(), Error> {
    let mut config = ctx.data().config.lock().await;

    match new_value {
        Some(v) => {
            let content;
            match utils::parse_on_off(&v) {
                Some(s) => {
                    config.features.introverted = s;
                    content = format!("new introverted setting: {}", v);
                }
                None => {
                    content = format!("invalid value for setting: {}", v);
                }
            };

            let reply = ctx.reply_builder(CreateReply::default());
            if !config.features.silent {
                ctx.send(
                    reply
                        .content(content)
                        .ephemeral(config.features.introverted)
                ).await?;
            }
        }
        None => {
            let reply = format!("current introverted setting: {}", config.features.introverted);
            ctx.say(reply).await?;
        }
    }

    Ok(())
}

#[poise::command(slash_command)]
pub async fn silent(
    ctx: Context<'_>,
    #[description = "new value"]
    #[autocomplete = "autocomplete_bool"]
    new_value: Option<String>
) -> Result<(), Error> {
    let mut config = ctx.data().config.lock().await;

    match new_value {
        Some(v) => {
            let content;
            match utils::parse_on_off(&v) {
                Some(s) => {
                    config.features.silent = s;
                    content = format!("new silent setting: {}", v);
                }
                None => {
                    content = format!("invalid value for setting: {}", v);
                }
            };

            let reply = ctx.reply_builder(CreateReply::default());
            if !config.features.silent {
                ctx.send(
                    reply
                        .content(content)
                        .ephemeral(config.features.introverted)
                ).await?;
            }

        }
        None => {
            let reply = format!("current silent setting: {}", config.features.silent);
            ctx.say(reply).await?;
        }
    }

    Ok(())
}