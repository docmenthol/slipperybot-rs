use crate::{get_default_activity, types::{Context, Error}};
use poise::serenity_prelude::ActivityData;
use poise::Context::Application;
use log::trace;

#[poise::command(slash_command)]
pub async fn ask(
    ctx: Context<'_>,
    #[description = "What"]
    what: String
) -> Result<(), Error> {
    if let Application(ctx) = ctx {
        trace!("user {} asked {}", ctx.interaction.user.name, what);

        let thinking = ActivityData::custom("thinking up something stupid...");
        ctx.serenity_context.set_activity(Some(thinking));

        let mut chain = ctx.data().chain.lock().await;

        if let Some(response) = chain.generate(&what)? {
            ctx.reply(format!("{} asked: \"{}\"\n{}", ctx.interaction.user.name, what, response)).await?;
        } else {
            ctx.reply("sorry, i couldn't generate a reply.").await?;
        }

        ctx.serenity_context.set_activity(Some(get_default_activity()));
    }

    Ok(())
}

#[poise::command(slash_command)]
pub async fn stats(
    ctx: Context<'_>
) -> Result<(), Error> {
    if let Application(ctx) = ctx {
        trace!("user {} wants brain stats", ctx.interaction.user.name);

        let thinking = ActivityData::custom("generating stats...");
        ctx.serenity_context.set_activity(Some(thinking));

        let chain = ctx.data().chain.lock().await;
        let stats = chain.stats();

        ctx.reply(format!("i know {} words.", stats.get_total_words())).await?;

        ctx.serenity_context.set_activity(Some(get_default_activity()));
    }

    Ok(())
}