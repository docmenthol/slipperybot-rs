use crate::types::{Context, Error};
use backend::{types::Preposition, utils::parse_preposition};
use futures::{Stream, StreamExt};
use std::time::{SystemTime, UNIX_EPOCH};
use poise::Context::Application;
use poise::CreateReply;
use log::trace;

async fn autocomplete_duration_preposition<'a>(
    _ctx: Context<'_>,
    partial: &'a str,
) -> impl Stream<Item = String> + 'a {
    futures::stream::iter(&["in", "at"])
        .filter(move |name| futures::future::ready(name.starts_with(partial)))
        .map(|name| name.to_string())
}

#[poise::command(prefix_command, slash_command)]
pub async fn reminder(
    ctx: Context<'_>,
    #[description = "Preposition"]
    #[autocomplete = "autocomplete_duration_preposition"]
    preposition: String,
    #[description = "When"]
    when: String,
    #[description = "What"]
    what: String
) -> Result<(), Error> {
	if let Application(ctx) = ctx {
		trace!("reminder for user {}", ctx.interaction.user.id);

		let reply = ctx.reply_builder(CreateReply::default());
		let epoch = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();

		let p;
		if let Some(prep) = parse_preposition(preposition.as_str()) {
			p = prep;
		} else {
			ctx.send(
				reply
					.content("Preposition must be 'in' or 'at'.")
					.ephemeral(true)
			).await?;
			return Ok(());
		}

		let reminder_time = match p {
			Preposition::In => {
				if let Ok(when) = parse_duration::parse(when.as_str()) {
					epoch + when
				} else {
					ctx.send(
						reply
							.content("Could not parse duration.")
							.ephemeral(true)
					).await?;
					return Ok(());
				}
			},
			Preposition::At => epoch,
		};

		let mut reply = reply
			.content(format!("Reminder set for <t:{}:F>: {}", reminder_time.as_secs(), what));

		let config = ctx.data().config.lock().await;
		if config.features.introverted {
			reply = reply.ephemeral(true);
		}

		ctx.send(reply).await?;
	}

	Ok(())
}
