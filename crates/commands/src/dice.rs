use crate::{get_default_activity, types::{Context, Error}};
use poise::{serenity_prelude::ActivityData, Context::Application};
use ndm::Dice;

#[poise::command(slash_command)]
pub async fn roll(
    ctx: Context<'_>,
    #[description = "What"]
    what: String
) -> Result<(), Error> {
	if let Application(ctx) = ctx {
		let thinking = ActivityData::custom("rolling dice...");
        ctx.serenity_context.set_activity(Some(thinking));

		if let Ok(roll) = &what.parse::<Dice>() {
			let reply = format!("rolled {}", roll);
			ctx.reply(reply).await?;
		} else {
			ctx.reply(format!("failed to roll {}", what)).await?;
		}

		ctx.serenity_context.set_activity(Some(get_default_activity()));
	}

	Ok(())
}