Dragon Ball Z is a Japanese anime television series produced by Toei Animation.
It is the sequel to Dragon Ball and adapts the latter 325 chapters of the original 519-chapter Dragon Ball manga series created by Akira Toriyama which ran in Weekly Shonen Jump from 1984 to 1995.
Dragon Ball Z aired in Japan on Fuji TV from April 1989 to January 1996, before getting subtitled or dubbed in territories including the United States, Canada, Australia, Europe, Asia, India and Latin America.
It was broadcast in at least 81 countries worldwide.
It is part of the Dragon Ball media franchise.
Dragon Ball Z continues the adventures of Son Goku, who, along with his companions, defends the Earth against villains ranging from aliens (Vegeta, Frieza), androids (Cell) and magical creatures (Majin Buu).
While the original Dragon Ball anime followed Goku from childhood to early adulthood, Dragon Ball Z is a continuation of his adult life, but at the same time parallels the life of his son, Gohan, as well as the development of his rivals, Piccolo and Vegeta.
Due to the success of the anime in the United States, the manga chapters making up its story were initially released by Viz Media under the Dragon Ball Z title.
Dragon Ball Z is adapted from the final 324 chapters of the manga series which were published in Weekly Shonen Jump from 1988 to 1995.
It premiered in Japan on Fuji Television on April 26, 1989, taking over its predecessor's time slot, and ran for 291 episodes until its conclusion on January 31, 1996.
Throughout the production, the voice actors were tasked with playing different characters and performing their lines on cue, switching between roles as necessary.
The voice actors were unable to record the lines separately because of the close dialogue timing.
When asked if juggling the different voices of Goku, Gohan and Goten were difficult, Masako Nozawa said that it was not and that she was able to switch roles simply upon seeing the character's picture.
She did admit that when they were producing two films a year and television specials in addition to the regular series, there were times when they had only line art to look at while recording, which made giving finer nuanced details in her performance difficult.
In 1996, Funimation Productions licensed Dragon Ball Z for an English-language release in North America, after canceling their initial dub of Dragon Ball half-way through their originally-planned 26-episode first season.
They worked with Saban Entertainment to syndicate the series on television, and Pioneer Entertainment to handle home video distribution.
Pioneer also ceased its home video release of the series at volume 17 (the end of the dub) and retained the rights to produce an uncut subtitled version, but did not do so.
They did, however, release uncut dubs of the first three Z movies on home video.
In 2005, Funimation began to re-dub episodes 1-67 with their in-house voice cast, including content originally cut from their dub with Saban.
This dub's background score was composed by Nathan M. Johnson.
Funimation had ceased working with Faulconer Productions after the final episode of Dragon Ball Z in 2003.
Funimation's new uncut dub of these episodes aired on Cartoon Network as part of its Monday-Thursday late night time slot, beginning in June 2005. Funimation's later remastered DVDs of the series saw them redub portions of the dialogue, mostly after episode 67, and had the option to play the entire series' dub with both the American and Japanese background music.
In January 2011, Funimation and Toei announced that they would stream Dragon Ball Z within 30 minutes before their simulcast of One Piece. As of 2017, Dragon Ball Z is no longer being streamed on Hulu.
The Funimation dubbed episodes also aired in Canada, Ireland, the United Kingdom, the Netherlands, Belgium, Australia, and New Zealand.
However, beginning with episode 108 (123 uncut), Westwood Media, in association with Ocean Studios, produced an alternate English dub, distributed to Europe by AB Groupe.
The alternate dub was created for broadcast in the UK, the Netherlands and Ireland, although it also aired in Canada beginning from episode 168 (183 uncut) to fulfill Canadian content requirements.
Funimation's in-house dub continued to air in the U.S., Australia, and New Zealand.
The Westwood Media production used the same voice cast from the original 53-episode dub produced by Funimation, it featured an alternate soundtrack by Tom Keenlyside and John Mitchell, though most of this score was pieces Ocean reused from other productions Keenlyside and Mitchell had scored for them, and it used the same scripts and video master as the TV edit of Funimation's in-house dub.
The Westwood Media dub never received a home video release. In Australia, Dragon Ball Z was broadcast by the free-to-air commercial network, Network Ten during morning children's programming, Cheez TV, originally using the censored Funimation/Saban dub before switching to Funimation's in-house dub.
Dragon Ball Z originally aired on the British Comedy Network in Fall 1998.
