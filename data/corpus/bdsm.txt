some men like to be submissive, in bdsm, and other sexual activities.
This is known as male submission, or malesub.
The woman who takes the dominant role, is usually called dominatrix.
Such activity is usually called femdom.
A 2015 study indicates that 46.6% of men who are active in BDSM expressed a preference for a submissive role, 24% consider themselves to be switches (they can take both roles) and 29.5% prefer the dominant role.
BDSM is an initialism for bondage/discipline (BD) and sadism/masochism (SM).
It means some kinds of sex play.
People have seen that they can get sexual stimulation by taking certain roles.
Some of these roles are being dominant, and telling the other person what to do, or of being submissive, and being told what to do.
Before they start, people agree that is what they want to do, so the activity is called consensual.
Usually, they also agree on a safe word, or some behavior, which will make the other person stop.
This is completely different from a sadist, who only gets pleasure from being sadistic or cruel to others, or from people only getting sexually aroused if they are hurt or humiliated.
People involved in BDSM agree that's what they want to do.
BDSM is very varied, it includes tying or restraining one person, animal roleplay, and using tools to get arousal (for example: a dildo), which is called fetishism.
BDSM is about getting sexual pleasure out of things that are often painful or upsetting.
People try to do this in a safe way by agreeing on a "safe word". If someone says this word the whole play stops. This is to stop the play from going too far and causing real physical or emotional hurt.
The term BDSM is first recorded in a Usenet post from 1991.
The terms themselves are older, though. Heinrich Kaan (1816-1893) published a book called Psychopathia sexualis in 1843.
Kaan was a medical doctor, and in his work, he looks at the sins listed in the Bible.
He was the first to use the terms perversion, aberration and deviation in a medical context.
In 1890, Richard von Krafft-Ebing (1840-1902) introduced the terms sadism and masochism.
In 1905, Sigmund Freud wrote that sadism and masochism were due to errors in development of children.
In 1913, Isidor Sadger first used the term "sado-masochism". 
Bondage is the use of items like handcuffs, ropes or chains to keep a willing person from moving.
Bondage often has to do with sex, but not always.
Bondage is done because some people like the feeling of being not able to move while having sex.
In this case, bondage sometimes has to do with BDSM, as often in the case of rope bondage and bondage of the female breasts.
The letter "B" in BDSM stands for "bondage".
Bondage may also be done just because people may like the feelings it creates.
Some couples include bondage as foreplay in their otherwise traditional sex lives at some time during their relationship.
These bedroom bondage games are often with one partner willingly being restrained with rope or cuffs.
Sometimes they can also be blindfolded or gagged.
Domination and submission is a lifestyle.
Often it is seen as a form of erotic play.
Usually two people do this.
With this lifestyle or context, one of the two people has the dominant role.
He or she can tell the other (called submissive) to do things.
The submissive has to obey.
The roles are usually agreed on beforehand.
The submission is voluntary.
Sadomasochism may be seen as a variation of domination and submission.
A person who might play either role is called a switch.
Hogtie bondage is a form of bondage in which the limbs of a person are tied together.
This make the person unable to move and helpless.