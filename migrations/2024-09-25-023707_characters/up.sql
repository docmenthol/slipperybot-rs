CREATE TABLE IF NOT EXISTS `characters` (
    `uid` UNSIGNED BIGINT UNIQUE PRIMARY KEY NOT NULL,
    `class` INTEGER NOT NULL,
    `activity` VARCHAR NOT NULL DEFAULT "0,0",
    `xp` INTEGER NOT NULL DEFAULT 0,
    `hp` INTEGER NOT NULL DEFAULT 10,
    `max_hp` INTEGER NOT NULL DEFAULT 10,
    `mana` INTEGER NOT NULL DEFAULT 10,
    `max_mana` INTEGER NOT NULL DEFAULT 10,
    `str` INTEGER NOT NULL,
    `agi` INTEGER NOT NULL,
    `dex` INTEGER NOT NULL,
    `int` INTEGER NOT NULL
);
