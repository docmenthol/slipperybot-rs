# $env:RUSTFLAGS = "-Zlocation-detail=none -Ztrim-paths=all -Zfmt-debug=none"
$env:RUSTFLAGS = "-Zlocation-detail=none"

cargo +nightly build `
	-Z build-std=std,panic_abort `
	-Z build-std-features="optimize_for_size" `
	--target x86_64-pc-windows-gnu --release
