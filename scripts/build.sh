# RUSTFLAGS="-Zlocation-detail=none -Ztrim-paths=all -Zfmt-debug=none"
RUSTFLAGS="-Zlocation-detail=none"
export RUSTFLAGS

cargo +nightly build `
	-Z build-std=std,panic_abort `
	-Z build-std-features="optimize_for_size" `
	--target x86_64-unknown-linux-gnu --release
