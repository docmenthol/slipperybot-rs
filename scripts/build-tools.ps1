# $env:RUSTFLAGS = "-Zlocation-detail=none -Ztrim-paths=all -Zfmt-debug=none"
$env:RUSTFLAGS = "-Zlocation-detail=none"

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-audio

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-corpus

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-crush

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-db

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-ha

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-interact

cargo +nightly build `
    -Z build-std=std,panic_abort `
    -Z build-std-features="optimize_for_size" `
    --target x86_64-pc-windows-gnu --release --package sb-train
