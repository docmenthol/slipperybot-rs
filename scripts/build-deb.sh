#!/usr/bin/env bash

# Compile slipperybot + tools
bash build.sh
bash build-tools.sh

# Create Debian package target
mkdir -p ./deb/{bin,etc,DEBIAN}
mkdir -p ./deb/usr/share/{doc/slipperybot,man/man1}

# Generate changelog and gzip it
git log > ./deb/usr/share/doc/slipperybot/changelog.Debian
gzip --best ./deb/usr/share/doc/slipperybot/changelog.Debian

# Mark conf
echo /etc/data/Config.toml > ./deb/DEBIAN/conffiles

# Gzip manpage
gzip --best -r ./assets/slipperybot.1 > ./deb/usr/share/man/man1/slipperybot.1
gzip --best -r ./assets/sb-corpus.1 > ./deb/usr/share/man/man1/sb-corpus.1
gzip --best -r ./assets/sb-crush.1 > ./deb/usr/share/man/man1/sb-crush.1
gzip --best -r ./assets/sb-db.1 > ./deb/usr/share/man/man1/sb-db.1
gzip --best -r ./assets/sb-interact.1 > ./deb/usr/share/man/man1/sb-interact.1
gzip --best -r ./assets/sb-train.1 > ./deb/usr/share/man/man1/sb-train.1

# Copy artifacts
cp data/Config.toml ./deb/etc/
cp ./target/armv7-unknown-linux-gnueabihf/release/{slipperybot,sa-corpus,sa-crush,sa-db,sa-interact,sa-train} ./deb/bin/
cp ./assets/control ./deb/DEBIAN/
cp ./assets/copyright ./deb/usr/share/doc/slipperybot/

# Build package
dpkg-deb --root-owner-group --build deb